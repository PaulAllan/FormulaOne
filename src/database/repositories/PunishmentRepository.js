const BaseRepository = require('./BaseRepository.js');
const PunQuery = require('../queries/PunQuery.js');
const Pun = require('../models/Pun.js');

class PunishmentRepository extends BaseRepository {
    anyPun(userId, guildId) {
        return this.any(new PunQuery(userId, guildId));
    }

    insertPun(userId, guildId) {
        return this.insertOne(new Pun(userId, guildId, 432000000)); // 432000000 = 5 days.
    }

    findPun(userId, guildId) {
        return this.find(new PunQuery(userId, guildId));
    }

    deletePun(userId, guildId) {
        return this.deleteOne(new PunQuery(userId, guildId));
    }
}

module.exports = PunishmentRepository;