const patron = require('patron.js');
const StringUtil = require('../../utility/StringUtil.js');
const ModerationService = require('../../services/ModerationService.js');
const Constants = require('../../utility/Constants.js');
const db = require('../../database/index.js');

class Ban extends patron.Command {
    constructor() {
        super({
            names: ['ban'],
            groupName: 'administrator',
            description: 'Ban a user.',
            args: [
                new patron.Argument({
                    name: 'user',
                    key: 'user',
                    type: 'user',
                    example: 'coco-bun#6681',
                    preconditions: ['nomoderatorban']
                }),
                new patron.Argument({
                    name: 'reason',
                    key: 'reason',
                    type: 'string',
                    example: 'Posting an IP logger.',
                    remainder: true
                })
            ]
        });
    }

    async run(msg, args, sender) {
        if (msg.guild.members.has(args.user.id)) {
            sender.dm(args.user, 'A moderator has banned you' + (StringUtil.isNullOrWhiteSpace(args.reason) ? '.' : ' for the reason: ' + args.reason + '.'));
            await db.userRepo.upsertUser(args.user.id, msg.guild.id, { $inc: { bans: 1 } });
        }
        await msg.guild.ban(args.user);
        await sender.reply('Successfully banned ' + StringUtil.boldify(args.user.tag) + '.');
        return ModerationService.tryModLog(msg.dbGuild, msg.guild, 'Ban', Constants.banColor, args.reason, msg.author, args.user);
    }
}

module.exports = new Ban();