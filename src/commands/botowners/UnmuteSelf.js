const patron = require('patron.js');

class UnmuteSelf extends patron.Command {
    constructor() {
        super({
            names: ['unmuteself'],
            groupName: 'botowners',
            description: 'Unmute yourself.'
        });
    }

    async run(msg, args, sender) {
        await sender.reply('Successfully unmuted yourself.');
        return msg.member.removeRole(msg.dbGuild.roles.muted);
    }
}

module.exports = new UnmuteSelf();