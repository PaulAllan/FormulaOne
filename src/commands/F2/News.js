const patron = require('patron.js');
const Constants = require('../../utility/Constants.js');
const ModerationService = require('../../services/ModerationService.js');

class News extends patron.Command {
    constructor() {
        super({
            names: ['news'],
            groupName: 'f2',
            description: 'Send a news link to #news.',
            args: [
                new patron.Argument({
                    name: 'link',
                    key: 'link',
                    type: 'string',
                    example: 'https://www.bbc.com/sport/formula1/44930765'
                })
            ]
        });
    }

    async run(msg, args, sender) {
        const text = Constants.blacklistedNews;
        const newsChannel = msg.guild.channels.get('335167453350854666');
        const pattern = /(http|https):\/\/(\w+:{0,1}\w*)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%!\-\/]))?/;
        if (!pattern.test(args.link)) {
            return sender.reply('That is not a valid URL.', { color: Constants.errorColor });
        }
        if (args.link.includes('youtube')) {
            return sender.reply('You are not allowed to post YouTube links.', { color: Constants.errorColor });
        }
        for (let i = 0; i < text.length; i++) {
            if (args.link.includes(text[i])) {
                await msg.delete();
                await msg.member.addRole(msg.dbGuild.roles.muted);
                await ModerationService.tryModLog(msg.dbGuild, msg.guild, 'Automatic Mute', Constants.kickColor, 'Attempted to post a blacklisted link to ' + newsChannel, null, msg.author, 'Link', args.link);
                return sender.dm(msg.author, 'You have been automatically muted for attempting to post a blacklisted link into #news, if you believe this is an error please contact a Moderator.');
            }
        }
        await newsChannel.send(`${args.link} sent by ${msg.author.tag}`);
        return sender.reply('Successfully posted to ' + newsChannel);
    }
}

module.exports = new News();
