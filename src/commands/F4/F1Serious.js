const patron = require('patron.js');

class F1Serious extends patron.Command {
  constructor() {
    super({
      names: ['serious', 'f1serious', 'formula1serious', 'formulaoneserious'],
      groupName: 'f4',
      description: 'Add/remove the F1 Serious role which grants access to #f1-serious.'
    });
  }

  async run(msg, args, sender) {
    if (!msg.guild.available) {
      return sender.reply('The Guild is currently busy, please try again later.');
    }
    const member = msg.member;
    const f1serious = msg.guild.roles.get('433231596095537152');
    if (member.roles.has(f1serious.id)) {
      await member.removeRole(f1serious.id);
      return sender.reply('Successfully removed F1 Serious.');
    }
    await member.addRole(f1serious.id);
    return sender.reply('Successfully added F1 Serious.');
  }
}

module.exports = new F1Serious();
