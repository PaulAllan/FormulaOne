const patron = require('patron.js');
const StringUtil = require('../../utility/StringUtil.js');
const Try = require('../../utility/Try.js');
const ModerationService = require('../../services/ModerationService.js');
const Constants = require('../../utility/Constants.js');

class Kick extends patron.Command {
    constructor() {
        super({
            names: ['kick'],
            groupName: 'moderation',
            description: 'Kick a member.',
            args: [
                new patron.Argument({
                    name: 'member',
                    key: 'member',
                    type: 'member',
                    example: 'coco-bun#6681',
                    preconditions: ['nomoderator']
                }),
                new patron.Argument({
                    name: 'reason',
                    key: 'reason',
                    type: 'string',
                    example: 'Breaking the rules.',
                    remainder: true
                })
            ]
        });
    }

    async run(msg, args, sender) {
        Try(sender.dm(args.member, 'A moderator has kicked you' + (StringUtil.isNullOrWhiteSpace(args.reason) ? '.' : ' for the reason: ' + args.reason + '.'), { footer: msg.guild.name }));
        await args.member.kick();
        await sender.reply('Successfully kicked ' + StringUtil.boldify(args.member.user.tag) + '.');
        return ModerationService.tryModLog(msg.dbGuild, msg.guild, 'Kick', Constants.kickColor, args.reason, msg.author, args.member.user);
    }
}

module.exports = new Kick();
