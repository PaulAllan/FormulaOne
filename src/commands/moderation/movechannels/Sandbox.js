const patron = require('patron.js');
const Constants = require('../../../utility/Constants.js');
const Sender = require('../../../utility/Sender.js');

class Sandbox extends patron.Command {
    constructor() {
        super({
            names: ['sb', 'sandbox'],
            groupName: 'moderation',
            description: 'Send the move to #offtrack message to #formula1.'
        });
    }

    async run(msg, args, sender) {
        if (!msg.guild.available) {
            return sender.reply('The Guild is currently busy, please post the message manually.', { color: Constants.errorColor });
        }
        const f1Channel = msg.guild.channels.get(Constants.formula1Channel);
        const sandboxChannel = msg.guild.channels.get(Constants.channels.sandbox);
        if (msg.channel.id === f1Channel.id) {
            msg.delete();
        } else {
            await sender.reply('Successfully sent the message.');
        }
        return Sender.send(f1Channel, 'Please move to ' + sandboxChannel + '.');
    }
}

module.exports = new Sandbox();
