const patron = require('patron.js');
const ModerationService = require('../../services/ModerationService.js');
const Constants = require('../../utility/Constants.js');

class Clear extends patron.Command {
    constructor() {
        super({
            names: ['clear', 'prune', 'clearchat', 'chatclear', 'purge'],
            groupName: 'moderation',
            description: 'Clear up to ' + Constants.clear.max + ' messages in any text channel.',
            botPermissions: ['MANAGE_MESSAGES'],
            preconditions: ['clear'],
            args: [
                new patron.Argument({
                    name: 'messages to clear',
                    key: 'quantity',
                    type: 'float',
                    example: '5'
                }),
                new patron.Argument({
                    name: 'reason',
                    key: 'reason',
                    type: 'string',
                    example: 'Spamming.',
                    defaultValue: '',
                    remainder: true
                })
            ]
        });
    }

    async run(msg, args, sender) {
        const messages = await msg.channel.fetchMessages({ limit: args.quantity + 1 });

        await msg.channel.bulkDelete(messages);

        const reply = await sender.reply('Successfully cleared ' + args.quantity + (args.quantity === 1 ? ' message' : ' messages') + '.');

        await ModerationService.tryModLog(msg.dbGuild, msg.guild, 'Clear', Constants.clearColor, args.reason, msg.author, null, 'Messages Cleared', args.quantity, 'Channel', msg.channel);

        return reply.delete(3000);
    }
}

module.exports = new Clear();