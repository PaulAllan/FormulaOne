const patron = require('patron.js');
const ModerationService = require('../../services/ModerationService.js');
const db = require('../../database/index.js');

class RepeatMessage extends patron.Command {
    constructor() {
        super({
            names: ['repeatmessage'],
            groupName: 'moderation',
            description: 'Toggle the repeating message for <#186021984126107649>'
        });
    }

    async run(msg, args, sender) {
        if (msg.dbGuild.repeatMessageEnabled) {
            await ModerationService.tryLogAnything(msg.dbGuild, msg.guild, msg.author, 'Disabled the repeating message <#186021984126107649>.');
            await db.guildRepo.upsertGuild(msg.guild.id, { $set: { 'repeatMessageEnabled': false } });
            return sender.reply('Successfully disabled the repeating message.');
        }
        await ModerationService.tryLogAnything(msg.dbGuild, msg.guild, msg.author, 'Enabled the repeating message in <#186021984126107649>.');
        await db.guildRepo.upsertGuild(msg.guild.id, { $set: { 'repeatMessageEnabled': true } });
        return sender.reply('Successfully enabled the repeating message.');
    }
}

module.exports = new RepeatMessage();
