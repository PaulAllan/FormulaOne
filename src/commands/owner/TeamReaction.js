const patron = require('patron.js');
const db = require('../../database/index.js');

class TeamReaction extends patron.Command {
    constructor() {
        super({
            names: ['teamreaction', 'tr', 'react'],
            groupName: 'owner',
            description: 'Sends a message that people react to gain / remove roles.'
        });
    }

    async run(msg, args, sender) {
        await msg.delete();
        const reply = await sender.send('Click on the reactions below to assign your teams!', { title: 'Choose your teams' });
        await db.guildRepo.upsertGuild(msg.guild.id, { $set: { 'reactionMessageId': reply.id } });
        await reply.react('247267220927938560'); // Ferrari
        await reply.react('247269330494947328'); // Mercedes
        await reply.react('247288822805561345'); // Red Bull
        await reply.react('247287077081841664'); // Renault
        await reply.react('247271624666120192'); // Haas
        await reply.react('247269312530874368'); // Force India
        await reply.react('247281683424870401'); // McLaren
        await reply.react('247291089998970880'); // Toro Rosso
        await reply.react('247285103108161536'); // Sauber
        return reply.react('299192398443970560'); // Williams
    }
}

module.exports = new TeamReaction();
