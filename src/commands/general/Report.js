const patron = require('patron.js');
const Constants = require('../../utility/Constants.js');
const ModerationService = require('../../services/ModerationService.js');

class Report extends patron.Command {
    constructor() {
        super({
            names: ['report'],
            groupName: 'general',
            description: 'Send a report to the Moderators.',
            args: [
                new patron.Argument({
                    name: 'reason',
                    key: 'reason',
                    type: 'string',
                    example: 'Racism arguments.',
                    remainder: true
                })
            ]
        });
    }

    async run(msg, args, sender) {
        await msg.delete();
        const reply = await sender.send('Successfully reported.');
        await reply.delete(3000);
        return ModerationService.tryReportLog(msg.guild, args.reason, msg.author, msg.channel, Constants.kickColor);
    }
}

module.exports = new Report();
