const DateUtil = require('../../utility/DateUtil');
const patron = require('patron.js');
const StringUtil = require('../../utility/StringUtil.js');
const ModerationService = require('../../services/ModerationService.js');

class UserInfo extends patron.Command {
    constructor() {
        super({
            names: ['userinfo', 'uinfo', 'memberinfo', 'minfo'],
            groupName: 'general',
            description: 'Check a member\'s information.',
            args: [
                new patron.Argument({
                    name: 'member',
                    key: 'member',
                    type: 'member',
                    defaultValue: '',
                    example: 'John#5974'
                })
            ]
        });
    }

    async run(msg, args, sender) {
        let member;
        args.member === '' ? member = msg.member : member = args.member;
        const awaited = Array.from(member.roles);
        let roles = [];
        let message = '';
        for (let i = 0; i < awaited.length; i++) {
            roles.push(awaited[i][0]);
        }
        for (let i = 1; i < roles.length; i++) {
            message+= msg.guild.roles.get(roles[i]) + ' ';
        }
        const days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
        const months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
        const registerDate = member.user.createdAt;
        const joinDate = member.joinedAt;
        const options = {};
        options.author = {
            name: member.user.tag,
            icon: member.user.avatarURL
        };

        const registered = days[registerDate.getDay()] + " " + registerDate.getDate() + DateUtil.DateSuffix(registerDate.getDate()) + " " + months[registerDate.getMonth()] + " " +registerDate.getFullYear();
        const joined = days[joinDate.getDay()] + " " + joinDate.getDate() + DateUtil.DateSuffix(joinDate.getDate()) + " " + months[joinDate.getMonth()] + " " + joinDate.getFullYear();
        return sender.sendFields(
            [
            'Status', StringUtil.upperFirstChar(member.user.presence.status), 'User ID', member.user.id,
            'Registered', registered, 'Joined', joined,
            `Roles (${roles.length - 1})`, message, 'Permission Level', ModerationService.getPermLevelStr(msg.dbGuild, member)
        ], options);
    }
}

module.exports = new UserInfo();
