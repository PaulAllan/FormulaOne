const patron = require('patron.js');
const Constants = require('../../utility/Constants.js');

class AssignRole extends patron.Command {
    constructor() {
        super({
            names: ['rank', 'team', 'role', 'assignrole', 'unassignrole'],
            groupName: 'general',
            description: 'Assign or unassign an assignable role.',
            botPermissions: ['MANAGE_ROLES'],
            args: [
                new patron.Argument({
                    name: 'role',
                    key: 'role',
                    type: 'role',
                    example: 'Ferrari',
                    preconditions: ['hierarchy'],
                    remainder: true
                })
            ]
        });
    }

    async run(msg, args, sender) {
        const roles = msg.dbGuild.roles.assignable;

        for (let i = 0; i < roles.length; i++) {
            if (args.role.id === roles[i].id) {
                if (msg.member.roles.has(args.role.id)) {
                    await msg.member.removeRole(args.role);
                    return sender.reply('Successfully unassigned role ' + args.role);
                }
                await msg.member.addRole(args.role);
                return sender.reply('Successfully assigned role ' + args.role);
            }
        }
        return sender.reply(args.role + ' is not assignable.', { color: Constants.errorColor });
    }
}

module.exports = new AssignRole();