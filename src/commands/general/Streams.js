const patron = require('patron.js');

class Streams extends patron.Command {
    constructor() {
        super({
            names: ['streams'],
            groupName: 'general',
            description: 'View all of the motorsport streams.'
        });
    }

    async run(msg, args, sender) {
        if (msg.channel.id !== '186021984126107649') {
            return sender.send('https://www.reddit.com/r/mostorsportstreams/');
        }
    }
}

module.exports = new Streams();
