const patron = require('patron.js');
const Constants = require('../../utility/Constants.js');

class AssignableRoles extends patron.Command {
    constructor() {
        super({
            names: ['roles', 'ranks', 'assignableroles'],
            groupName: 'general',
            description: 'View all of the assignable roles.'
        });
    }

    async run(msg, args, sender) {
        if (msg.dbGuild.roles.assignable.length === 0) {
            return sender.reply('No assignable roles found.', { color: Constants.errorColor });
        }

        const roles = msg.dbGuild.roles.assignable;

        let description = '';

        for (let i = 0; i < roles.length; i++) {
            const rank = msg.guild.roles.get(roles[i].id);

            description += rank + '\n';
        }

        return sender.send(description, { title: 'Assignable Roles' });
    }
}

module.exports = new AssignableRoles();