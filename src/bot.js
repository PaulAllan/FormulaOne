const path = require('path');
const db = require('./database/index.js');
const client = require('./singletons/client.js');
const RequireAll = require('patron.js').RequireAll;
const Logger = require('./utility/Logger.js');
const credentials = require('./credentials.json');
const registry = require('./singletons/registry.js');
const reqAbs = require('./utility/reqAbs.js');
const IntervalService = require('./services/IntervalService.js');
const CountdownService = require('./services/CountdownService.js');

client.registry = registry;

RequireAll(path.join(__dirname, 'events'));
IntervalService.initiate();

(async () => {
  await db.connect(credentials.mongodbConnectionURL);
  const registry = require('./singletons/registry.js');
  registry.registerGlobalTypeReaders();
  registry.registerLibraryTypeReaders();
  registry.registerPreconditions(await reqAbs(__dirname, './preconditions/command'));
  registry.registerArgumentPreconditions(await reqAbs(__dirname, './preconditions/argument'));
  registry.registerGroups(await reqAbs(__dirname, './groups'));
  registry.registerCommands(await reqAbs(__dirname, './commands'));
  return client.login(credentials.token);
})()
  .catch((err) => Logger.handleError(err));
