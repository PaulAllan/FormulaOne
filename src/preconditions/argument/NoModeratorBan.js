const ModerationService = require('../../services/ModerationService.js');
const patron = require('patron.js');

class NoModeratorBan extends patron.ArgumentPrecondition {
    constructor() {
        super({
            name: 'nomoderatorban'
        });
    }
    async run(command, msg, argument, args, value) {
        const member = msg.guild.members.get(value.id);
        if (member === null || member === undefined) {
            return patron.PreconditionResult.fromSuccess();
        }
        if (ModerationService.getPermLevel(msg.dbGuild, member) === 0) {
            return patron.PreconditionResult.fromSuccess();
        }

        return patron.PreconditionResult.fromError(command, 'You may not use this command on a moderator.');
    }
}

module.exports = new NoModeratorBan();