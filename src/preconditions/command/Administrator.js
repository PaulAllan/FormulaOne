const patron = require('patron.js');
const ModerationService = require('../../services/ModerationService.js');
const ArrayUtil = require('../../utility/ArrayUtil.js');

class Administrator extends patron.Precondition {
    constructor() {
        super({
            name: 'administrator'
        });
    }

    async run(command, msg) {
        if (ModerationService.getPermLevel(msg.dbGuild, msg.guild.member(msg.author)) >= 2) {
            return patron.PreconditionResult.fromSuccess();
        }
        if (ModerationService.getPermLevel(msg.dbGuild, msg.guild.member(msg.author)) === 1) {
            const adminList = msg.guild.roles.get('424590836777484291').members.map(m => m.user.tag);
            const stewardList = msg.guild.roles.get('177408501268611073').members.map(m => m.user.tag);
            let all = adminList.concat(stewardList);
            all = await ArrayUtil.removeDuplicates(all);
            let message = '';
            for (let i = 0; i < all.length; i++) {
                message += all[i] + ',\n';
            }
            return patron.PreconditionResult.fromError(command, 'You must get a Steward to use this command.\n\n**Stewards:**\n' + message);
        }

        return patron.PreconditionResult.fromError(command, 'You must be a Steward in order to use this command.');
    }
}

module.exports = new Administrator();
