const patron = require('patron.js');

class Fantasy extends patron.Precondition {
    constructor() {
        super({
            name: 'fantasy'
        });
    }

    async run(command, msg) {
        if (msg.channel.id === '525995113499983883') {
            return patron.PreconditionResult.fromSuccess();
        }
        return patron.PreconditionResult.fromError(command, 'This command is exclusive to the fantasy user channel, use $rank fantasy-f1 to see where the fun is!');
    }
}

module.exports = new Fantasy();
