const patron = require('patron.js');

class F4Role extends patron.Precondition {
    constructor() {
        super({
            name: 'f4role'
        });
    }

    async run(command, msg) {
        const f4 = msg.guild.roles.get('313677111695245312');
        if (f4 === null || f4 === undefined) {
            return patron.PreconditionResult.fromError(command, 'Role is undefined, please contact Fozzie#0001.');
        }
        if (msg.member.roles.has(f4.id)) {
            return patron.PreconditionResult.fromSuccess();
        }

        return patron.PreconditionResult.fromError(command, 'You must have the F4 role in order to use this command.');
    }
}

module.exports = new F4Role();
