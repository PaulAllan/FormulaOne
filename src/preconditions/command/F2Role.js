const patron = require('patron.js');

class F2Role extends patron.Precondition {
    constructor() {
        super({
            name: 'f2role'
        });
    }

    async run(command, msg) {
        const f2 = msg.guild.roles.get('314910011358707712');
        if (f2 === null || f2 === undefined) {
            return patron.PreconditionResult.fromError(command, 'Role is undefined, please contact Fozzie#0001.');
        }
        if (msg.member.roles.has(f2.id)) {
            return patron.PreconditionResult.fromSuccess();
        }

        return patron.PreconditionResult.fromError(command, 'You must have the F2 role in order to use this command.');
    }
}

module.exports = new F2Role();
