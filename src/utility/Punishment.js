const db = require('../database/index.js');
const Constants = require('../utility/Constants.js');
const ModerationService = require('../services/ModerationService.js');
const StringUtil = require('../utility/StringUtil.js');

class Punishment {

    async increasePunishment(memberId, guildId) {
        const dbUser = await db.userRepo.getUser(memberId, guildId);
        const currentPun = dbUser.currentPunishment;
        if (currentPun === 0) {
            await db.punRepo.insertPun(memberId, guildId);
        }
        return db.userRepo.upsertUser(memberId, guildId, { $inc: { currentPunishment: 1 } });
    }

    async decreasePunishment(memberId, guildId) {
        const dbUser = await db.userRepo.getUser(memberId, guildId);
        return db.userRepo.upsertUser(memberId, guildId, { $inc: { currentPunishment: -1 } });
    }

    async runPunishCommand(msg, args, sender) {
        const date = new Date();
        const dbUser = await db.userRepo.getUser(args.member.id, msg.guild.id);
        const currentPun = await dbUser.currentPunishment;
        const readableDate = await new Date(date.getFullYear(), date.getMonth(), date.getDate(), date.getHours(), date.getMinutes(), date.getSeconds());
        const reasonSplit = args.reason.split(" ");
        const lastArg = reasonSplit.pop();
        let delArgs = false;
        let reason;
        if (lastArg.toLowerCase() === "-s") {
            msg.delete();
            delArgs = true;
            reason = reasonSplit.join(" ");
        } else {
            reason = args.reason;
        }
        let reply = null;

        if (currentPun === 0) {
            reply = await sender.send('Successfully warned ' + StringUtil.boldify(args.member.user.tag) + ' \n\nThey have ' + currentPun + ' punishments in the last 5 days.');
            await db.userRepo.upsertUser(args.member.id, msg.guild.id, { $inc: { warns: 1 } });
            await db.userRepo.upsertUser(args.member.id, msg.guild.id, new db.updates.Push('punishments', { date: Date.now(), readableDate: readableDate.toGMTString(), escalation: '1 (Warning)', reason: reason, mod: msg.author.tag, channelId: msg.channel.id }));
            await this.increasePunishment(args.member.id, msg.guild.id);
            await ModerationService.tryModLog(msg.dbGuild, msg.guild, 'Warn', Constants.warnColor, reason, msg.author, args.member.user, 'Punishments in last 5 days', currentPun);
            await sender.dm(args.member.user, 'A moderator has warned you' + (StringUtil.isNullOrWhiteSpace(reason) ? '.' : ' for the reason: ' + reason + '.' + '\n\nThis is your 1st punishment in 5 days. Next punishment is a 15 minute mute.'));
        }
        const role = msg.guild.roles.get(msg.dbGuild.roles.muted);
        if (currentPun === 1) {
            if (args.member.roles.has(msg.dbGuild.roles.muted)) {
                await db.muteRepo.deleteMute(args.member.id, msg.guild.id);
            }
            if (role === undefined) {
                return sender.reply('The muted role is not set, please get an Administrator (or Fozzie) to set it using $setmutedrole <Role>', { color: Constants.errorColor });
            }
            reply = await sender.send('Successfully muted ' + StringUtil.boldify(args.member.user.tag) + ' for 15 minutes.\n\nThey have ' + currentPun + ' punishment in the last 5 days.');
            await args.member.addRole(role);
            await this.increasePunishment(args.member.id, msg.guild.id);
            await db.muteRepo.insertMute(args.member.id, msg.guild.id, 900000);
            await db.userRepo.upsertUser(args.member.id, msg.guild.id, { $inc: { mutes: 1 } });
            await db.userRepo.upsertUser(args.member.id, msg.guild.id, new db.updates.Push('punishments', { date: Date.now(), readableDate: readableDate.toGMTString(), escalation: '2 (15 minute mute)', reason: reason, mod: msg.author.tag, channelId: msg.channel.id }));
            await ModerationService.tryModLog(msg.dbGuild, msg.guild, '15 minute Mute', Constants.muteColor, reason, msg.author, args.member.user, 'Punishments in last 5 days', currentPun);
            await sender.dm(args.member.user, 'A moderator has muted you for 15 minutes' + (StringUtil.isNullOrWhiteSpace(reason) ? '.' : ' for the reason: ' + reason + '.' + '\n\nThis is your 2nd punishment in 5 days. Next punishment is a 1 hour mute.'));
        }
        if (currentPun === 2) {
            if (args.member.roles.has(msg.dbGuild.roles.muted)) {
                await db.muteRepo.deleteMute(args.member.id, msg.guild.id);
            }
            if (role === undefined) {
                return sender.reply('The muted role is not set, please get an Administrator (or Fozzie) to set it using $setmutedrole <Role>', { color: Constants.errorColor });
            }
            reply = await sender.send('Successfully muted ' + StringUtil.boldify(args.member.user.tag) + ' for 1 hour.\n\nThey have ' + currentPun + ' punishments in the last 5 days.');
            await args.member.addRole(role);
            await this.increasePunishment(args.member.id, msg.guild.id);
            await db.muteRepo.insertMute(args.member.id, msg.guild.id, 3600000);
            await db.userRepo.upsertUser(args.member.id, msg.guild.id, { $inc: { mutes: 1 } });
            await db.userRepo.upsertUser(args.member.id, msg.guild.id, new db.updates.Push('punishments', { date: Date.now(), readableDate: readableDate.toGMTString(), escalation: '3 (1 hour mute)', reason: reason, mod: msg.author.tag, channelId: msg.channel.id }));
            await ModerationService.tryModLog(msg.dbGuild, msg.guild, '1 hour Mute', Constants.muteColor, reason, msg.author, args.member.user, 'Punishments in last 5 days', currentPun);
            await sender.dm(args.member.user, 'A moderator has muted you for 1 hour' + (StringUtil.isNullOrWhiteSpace(reason) ? '.' : ' for the reason: ' + reason + '.' + '\n\nThis is your 3rd punishment in 5 days. Next punishment is a 24 hour mute.'));
        }
        if (currentPun === 3) {
            if (args.member.roles.has(msg.dbGuild.roles.muted)) {
                await db.muteRepo.deleteMute(args.member.id, msg.guild.id);
            }
            if (role === undefined) {
                return sender.reply('The muted role is not set, please get an Administrator (or Fozzie) to set it using $setmutedrole <Role>', { color: Constants.errorColor });
            }
            reply = await sender.send('Successfully muted ' + StringUtil.boldify(args.member.user.tag) + ' for 5 days.\n\nThey have ' + currentPun + ' punishments in the last 5 days.');
            await args.member.addRole(role);
            await this.increasePunishment(args.member.id, msg.guild.id);
            await db.muteRepo.insertMute(args.member.id, msg.guild.id, 86400000);
            await db.userRepo.upsertUser(args.member.id, msg.guild.id, { $inc: { mutes: 1 } });
            await db.userRepo.upsertUser(args.member.id, msg.guild.id, new db.updates.Push('punishments', { date: Date.now(), readableDate: readableDate.toGMTString(), escalation: '4 (24 hour mute)', reason: reason, mod: msg.author.tag, channelId: msg.channel.id }));
            await ModerationService.tryModLog(msg.dbGuild, msg.guild, '24 hour Mute', Constants.muteColor, reason, msg.author, args.member.user, 'Punishments in last 5 days', currentPun);
            await sender.dm(args.member.user, 'A moderator has muted you for 5 days' + (StringUtil.isNullOrWhiteSpace(reason) ? '.' : ' for the reason: ' + reason + '.' + '\n\nThis is your 4th punishment in 5 days. Next punishment is at the Moderator\'s disgresion.'));
        }
        if (reply !== null) {
            if (delArgs) {
                await reply.delete(4000);
            }
        }
        if (currentPun > 3) {
            return sender.reply(StringUtil.boldify(args.member.user.tag) + ' has exceeded 4 punishments in the last 5 days, escalate their punishment manually.\n\nPunishments in the last 5 days: ' + currentPun, { color: Constants.errorColor });
        }
    }
}

module.exports = new Punishment();
