class Constants {
  constructor() {
    this.defaultColors = [
      0xff269a,
      0x00ff00,
      0x00e828,
      0x08f8ff,
      0xf226ff,
      0xff1C8e,
      0x68ff22,
      0xffbe11,
      0x2954ff,
      0x9624ed,
      0xa8ed00
    ];
    this.errorColor = 0xff0000;
    this.greenColor = 0x00e828;
    this.banColor = 0xea0c00;
    this.kickColor = 0xe8511f;
    this.muteColor = 0xff720e;
    this.unbanColor = 0x13ff19;
    this.unmuteColor = 0x6ded5e;
    this.warnColor = 0xffB620;
    this.banishColor = 0xf47A42;

    this.disabledEvents = [
      'CHANNEL_PINS_UPDATE',
      'VOICE_STATE_UPDATE',
      'TYPING_START',
      'VOICE_SERVER_UPDATE',
    ];

    this.game = '$help';
    this.prefix = '$';
    this.blockedMotorsports = ['formula e', 'formulae', 'world endurance championship', 'indy', 'nascar', 'motogp', 'dtm', 'wrc', 'supergt', 'superformula', 'wec', 'super gt', 'supercup', 'f2', 'f3', 'formula 2', 'formula 3', 'formula 4', 'f4'];
    this.formula1Channel = '186021984126107649';
    this.modLog = '447397947261452288';
    this.blacklistedNews = ['xvideos.com', 'xhamster.com', 'pornhub.com', 'xnxx.com', 'redtube.com', 'youporn.com', 'tube8.com', 'youjizz.com', 'brazzers.com', 'motherless.com'];

    this.channels = {
      motorsports: '247455736471224320',
      offtrack: '242392969213247500',
      banter: '432208507073331201',
      sandbox: '242392574193565711',
      mtv: '338155101577281537'
    };
    this.regexes = {
      markdown: /(\*|~|`|_)+/g,
      prefix: /^\$/,
    };

    this.intervals = {
      autoUnmute: 60000,
      resetPunishments: 600000,
      repeatMessage: 600000,
      lotto: 7200000,
      removeMemberRole: 60000
    };

    this.balance = {
      cooldown: 30000,
      minLength: 7,
      perMessage: 10
    };

    this.spam = {
      duration: 180,
      msgLimit: 3
    };

    this.clear = {
      min: 1,
      max: 199
    };

    this.teamRoles = {
      ferrari: '186069694451679232',
      mercedes: '186069675694751746',
      redbull: '186069703230226432',
      renault: '186069769177399296',
      haas: '186069763372351488',
      forceindia: '186069729130184704',
      mclaren: '186069741733937152',
      tororosso: '186069710545092608',
      sauber: '186071442947964928',
      williams: '186069719567040512'
    };

    this.commandSimilarity = 0.66;
  }
}

module.exports = new Constants();
