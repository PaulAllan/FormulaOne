const db = require('../database/index.js');
const Constants = require('../utility/Constants.js');
const ModerationService = require('../services/ModerationService.js');
const StringUtil = require('../utility/StringUtil.js');

class BanishUtil {

    async banishMember(msg, args, sender) {
        const role = msg.guild.roles.get(msg.dbGuild.roles.banished);
        const date = new Date();
        const readableDate = await new Date(date.getFullYear(), date.getMonth(), date.getDate(), date.getHours(), date.getMinutes(), date.getSeconds());
        if (args.member.roles.has(msg.dbGuild.roles.banished)) {
            return sender.reply(StringUtil.boldify(args.member.user.tag) + ' is already banished.', { color: Constants.errorColor });
        }
        if (role === undefined) {
            return sender.reply('The banished role is not set, please get an Administrator (or Fozzie) to set it using $setbanishrole <Role>', { color: Constants.errorColor });
        }
        await args.member.addRole(role.id);
        await ModerationService.tryModLog(msg.dbGuild, msg.guild, 'Banish', Constants.banishColor, args.reason, msg.author, args.member.user);
        await sender.reply('Successfully banished ' + StringUtil.boldify(args.member.user.tag) + '.');
        await db.userRepo.upsertUser(args.member.id, msg.guild.id, new db.updates.Push('punishments', { date: Date.now(), readableDate: readableDate.toGMTString(), escalation: 'Banish', reason: args.reason, mod: msg.author.tag, channelId: msg.channel.id }));
        return sender.dm(args.member.user, 'A moderator has banished you from all live channels.');
    }
}

module.exports = new BanishUtil();
