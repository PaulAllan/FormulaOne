// const Constants = require('../utility/Constants.js');
// const client = require('../singletons/client.js');
// const Logger = require('../utility/Logger.js');
//
// client.setInterval(() => {
//     (async function () {
//         await Logger.log('Interval: Remove New Members Role', 'DEBUG');
//
//         const guild = await client.guilds.get('177387572505346048');
//         const role = await guild.roles.get('546964268449267722');
//         const members = await role.members;
//
//         for (let i = 0; i < members.size; i++) {
//             const currentMember = await members[i];
//             const joinedAt = await currentMember.joinedAt.getMilliseconds();
//             if (joinedAt + 900000 > Date.now()) {
//                 continue;
//             }
//
//             if (members[i] === null) {
//                 continue;
//             }
//
//             if (role === undefined) {
//                 continue;
//             }
//
//             if (guild.me.hasPermission('MANAGE_ROLES') === false || role.position >= guild.me.highestRole.position) {
//                 continue;
//             }
//
//             await members[i].removeRole(role);
//         }
//     })()
//         .catch((err) => Logger.handleError(err));
// }, Constants.intervals.removeMemberRole);
