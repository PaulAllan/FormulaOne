const db = require('../database');
const Constants = require('../utility/Constants.js');
const ModerationService = require('../services/ModerationService.js');
const client = require('../singletons/client.js');
const Logger = require('../utility/Logger.js');

client.setInterval(() => {
    (async function () {
        await Logger.log('Interval: Reset Punishments', 'DEBUG');

        const puns = await db.punRepo.findMany();

        for (let i = 0; i < puns.length; i++) {
            if (puns[i].punishedAt + puns[i].punLength > Date.now()) {
                continue;
            }

            await db.punRepo.deleteById(puns[i]._id);

            const guild = client.guilds.get(puns[i].guildId);

            if (guild === undefined) {
                continue;
            }

            const member = guild.member(puns[i].userId);

            if (member === null) {
                continue;
            }

            const dbGuild = await db.guildRepo.getGuild(guild.id);

            await db.userRepo.upsertUser(member.id, guild.id, { $set: { currentPunishment: 0 } });
            await ModerationService.tryModLog(dbGuild, guild, 'Automatic Punishment Reset', Constants.unmuteColor, '', null, member.user);
        }
    })()
        .catch((err) => Logger.handleError(err));
}, Constants.intervals.resetPunishments);