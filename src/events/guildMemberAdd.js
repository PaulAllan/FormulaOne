const ModerationService = require('../services/ModerationService.js');
const Constants = require('../utility/Constants.js');
const db = require('../database');
const client = require('../singletons/client.js');

client.on('guildMemberAdd', async (member) => {
    const dbGuild = await db.guildRepo.getGuild(member.guild.id);
    // const newMemberRole = member.guild.roles.get('546964268449267722');

    // await member.addRole(newMemberRole);
    if (dbGuild.roles.muted !== null && await db.muteRepo.anyMute(member.id, member.guild.id) === true) {
        const role = member.guild.roles.get(dbGuild.roles.muted);

        if (!(role === undefined || member.guild.me.hasPermission('MANAGE_ROLES') === false || role.position >= member.guild.me.highestRole.position)) {
            return member.addRole(role);
        }
        const channel = await member.guild.channels.get(Constants.modLog);
        const marshalRole = await member.guild.roles.get('293845938764644352');
        await ModerationService.tryLogAnything(dbGuild, member.guild, null, member.user.tag + ' is muted and rejoined, I could not give them the Muted role, please do it manually.');
        return channel.send(marshalRole);
    }
});
