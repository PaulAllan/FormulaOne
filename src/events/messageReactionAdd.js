const Constants = require('../utility/Constants.js');
const client = require('../singletons/client.js');
const db = require('../database/index.js');
const Logger = require('../utility/Logger.js');

client.on('messageReactionAdd', async (messageReaction, user) => {
    if (user.bot) {
        return;
    }
    const msg = messageReaction.message;
    const dbGuild = await db.guildRepo.getGuild(msg.guild.id);
    const member = await msg.guild.members.get(user.id);
    if (msg.id === dbGuild.reactionMessageId) {
        const reaction = messageReaction.emoji.name;
        let roleId = null;
        switch (reaction) {
            case 'ferrari':
                roleId = Constants.teamRoles.ferrari;
                break;
            case 'mercedes':
                roleId = Constants.teamRoles.mercedes;
                break;
            case 'redbull':
                roleId = Constants.teamRoles.redbull;
                break;
            case 'renault':
                roleId = Constants.teamRoles.renault;
                break;
            case 'haas':
                roleId = Constants.teamRoles.haas;
                break;
            case 'forceindia':
                roleId = Constants.teamRoles.forceindia;
                break;
            case 'mclaren':
                roleId = Constants.teamRoles.mclaren;
                break;
            case 'tororosso':
                roleId = Constants.teamRoles.tororosso;
                break;
            case 'sauber':
                roleId = Constants.teamRoles.sauber;
                break;
            case 'williams':
                roleId = Constants.teamRoles.williams;
                break;
        }
        if (roleId === null || roleId === undefined) {
            return Logger.log(`WARNING - roleid is null or undefined.`, 'ERROR');
        }
        const role = msg.guild.roles.get(roleId);
        if (member.roles.has(roleId)) {
            return Logger.log(`${user.tag} (${user.id}) - Already has role ` + role.name, 'ERROR');
        }
        await member.addRole(role);
        return Logger.log(`${user.tag} (${user.id}) - Assigned role ` + role.name, 'DEBUG');
    }
});