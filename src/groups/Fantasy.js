const patron = require('patron.js');

class Fantasy extends patron.Group {
    constructor() {
        super({
            name: 'fantasy',
            description: 'The commands for the F1 Fantasy in #f1-fantasy-user.',
            preconditions: ['fantasy']
        });
    }
}

module.exports = new Fantasy();
